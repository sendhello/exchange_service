from decimal import Decimal
from typing import Optional

from pydantic import BaseModel, Field

from .rates import RateDb


class CoinBase(BaseModel):
    """Монета.
    """
    title: str = Field(title='Наименование монеты')


class CoinDb(CoinBase):
    id: int = Field(title='ID монеты')
    current_rate: Optional[Decimal] = Field(title='Текущий курс')

    class Config:
        orm_mode = True


class CoinWithRate(CoinBase):
    rates: list[RateDb] = Field(title='Изменение курса', default_factory=list)

from datetime import datetime

from sqlalchemy import insert  # type: ignore

from app.db.models import Coin, RateHistory


def get_coins_query() -> str:
    """Запрос списка монет с их текущим курсом.
    """
    return """
        WITH max_rate AS (
            SELECT rh.coin_id        as coin_id,
                   max(rh.timestamp) as max_timestamp
            FROM rate_history rh
            GROUP BY rh.coin_id
        )
        SELECT c.id    as id,
               c.title as title,
               rh.rate as current_rate
        FROM coins c
            LEFT JOIN max_rate mr on c.id = mr.coin_id
            LEFT JOIN rate_history rh on c.id = rh.coin_id AND rh.timestamp = mr.max_timestamp;
    """


def get_coin_query(coin_id: int, start_time: datetime, end_time: datetime) -> str:
    """Запрос истории курса.

    coin_id: ID монеты
    start_time: Дата и время начала запрашиваемого периода, включительно
    end_time: Дата и время конца запрашиваемого периода, не включительно
    """
    return f"""
        SELECT c.id              as id,
               c.title           as title,
               rh.timestamp      as timestamp,
               rh.rate           as rate
        FROM coins c
                 LEFT JOIN rate_history rh on c.id = rh.coin_id
        WHERE c.id = {coin_id}
        AND rh.timestamp >= '{start_time}'
        AND rh.timestamp < '{end_time}';
    """


def set_coins_query() -> str:
    return insert(Coin)


def set_rates_query() -> str:
    return insert(RateHistory)

import datetime

from sqlalchemy import TIMESTAMP, Column, ForeignKey, Integer, Numeric, String  # type: ignore
from sqlalchemy.orm import relationship  # type: ignore

from . import Base


class Coin(Base):
    __tablename__ = 'coins'

    id = Column(Integer, primary_key=True)
    title = Column(String, unique=True)
    rates = relationship('RateHistory', uselist=True, backref='coin')


class RateHistory(Base):
    __tablename__ = 'rate_history'

    id = Column(Integer, primary_key=True)
    coin_id = Column(Integer, ForeignKey('coins.id', ondelete='CASCADE'))
    timestamp = Column(TIMESTAMP, default=datetime.datetime.utcnow)
    rate = Column(Numeric(10, 2))

from fastapi import FastAPI

from app.api import api_router
from app.constants import PROJECT_NAME, __version__
from app.db import database

app = FastAPI(
    title=PROJECT_NAME,
    version=__version__,
)

app.include_router(api_router, prefix='/api/v1')


@app.on_event('startup')
async def startup() -> None:
    await database.connect()


@app.on_event('shutdown')
async def shutdown() -> None:
    await database.disconnect()

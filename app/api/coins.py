from datetime import datetime, timedelta
from typing import Any, Mapping

from fastapi import APIRouter, HTTPException, status

from app.db import database
from app.db.queries import get_coin_query, get_coins_query
from app.schemas.coins import CoinDb, CoinWithRate, RateDb

api_router = APIRouter()


@api_router.get('/', response_model=list[CoinDb])
async def get_coins() -> list[Mapping[Any, Any]]:
    """Возвращает все существующие монеты.
    """
    res = await database.fetch_all(
        query=get_coins_query(),
    )
    if not res:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Coins not found')

    return res


@api_router.get('/{coin_id}', response_model=CoinWithRate)
async def get_coin(coin_id: int, start_time: datetime = None, end_time: datetime = None) -> CoinWithRate:
    """Возвращает монету по ее ID.
    """
    if end_time is None:
        end_time = datetime.now()

    if start_time is None:
        start_time = end_time - timedelta(days=1)

    if start_time > end_time:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='"start_time" is upper "end_time"')

    db_rates = await database.fetch_all(
        query=get_coin_query(coin_id=coin_id, start_time=start_time, end_time=end_time),
    )
    if not db_rates:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f'Coind with id `{coin_id}` not found')

    coin_with_rate = CoinWithRate.parse_obj(db_rates[0])
    for db_rate in db_rates:
        coin_with_rate.rates.append(RateDb.parse_obj(db_rate))

    return coin_with_rate

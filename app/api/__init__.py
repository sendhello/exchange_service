from fastapi import APIRouter

from . import coins, healthcheck

api_router = APIRouter(
    responses={
        404: {"description": "Page not found"},
    },
)
api_router.include_router(coins.api_router, prefix='/coins')
api_router.include_router(healthcheck.api_router, prefix='/healthcheck')

import json
from datetime import datetime
from typing import Callable, Union

from fastapi.testclient import TestClient

import app.api.coins as coins
from app.main import app

client = TestClient(app)


def fake_response(fake_base_name: str) -> Callable:
    async def fetch_one(query: str) -> Union[list, dict]:
        with open(f'tests/app/data/{fake_base_name}.json') as file:
            res = json.load(file)
        return res

    return fetch_one


def test_app_get_points_ok(mocker) -> None:
    """get_coins: положительный кейс.
    """
    mocker.patch('app.api.coins.database.fetch_all', fake_response('get_coins_ok'))

    response = client.get('/api/v1/coins')
    assert response.status_code == 200, response.text
    data = response.json()
    assert data == [
        {
            'current_rate': 57,
            'id': 1,
            'title': 'ticker_00',
        },
        {
            'current_rate': 59,
            'id': 2,
            'title': 'ticker_01',
        },
        {
            'current_rate': 25,
            'id': 3,
            'title': 'ticker_02',
        },
        {
            'current_rate': 18,
            'id': 4,
            'title': 'ticker_03',
        },
        {
            'current_rate': 43,
            'id': 5,
            'title': 'ticker_04',
        },
    ]


def test_app_get_points_empty(mocker) -> None:
    """get_coins: нет ни одной монеты.
    """
    mocker.patch('app.api.coins.database.fetch_all', fake_response('get_coins_empty'))

    response = client.get('/api/v1/coins')
    assert response.status_code == 404, response.text
    data = response.json()
    assert data == {'detail': 'Coins not found'}


def test_app_get_point_id_ok(mocker) -> None:
    """get_coin: положительный кейс.

    Запрос по id
    """
    mocker.patch('app.api.coins.database.fetch_all', fake_response('get_coin_ok'))
    mocker.patch('app.api.coins.datetime')
    coins.datetime.now.return_value = datetime(2020, 7, 1, 12)

    response = client.get('/api/v1/coins/22')
    assert response.status_code == 200, response.text
    data = response.json()
    assert data == {
        'title': 'ticker_02',
        'rates': [
            {'rate': 1, 'timestamp': '2020-07-01T12:00:00'},
            {'rate': 0, 'timestamp': '2020-07-01T13:00:00'},
            {'rate': 1, 'timestamp': '2020-07-01T14:00:00'},
            {'rate': 2, 'timestamp': '2020-07-01T10:00:00'},
            {'rate': 1, 'timestamp': '2020-07-02T12:00:00'},
        ],
    }


def test_app_get_point_id_with_times_ok(mocker) -> None:
    """get_coin: положительный кейс.

    Запрос по id, start_time и end_time
    """
    mocker.patch('app.api.coins.database.fetch_all', fake_response('get_coin_ok'))
    mocker.patch('app.api.coins.datetime')
    coins.datetime.now.return_value = datetime(2020, 7, 1, 12)

    response = client.get('/api/v1/coins/22?start_time=2020-06-01T12:00:00&end_time=2020-07-01T12:00:00')
    assert response.status_code == 200, response.text
    data = response.json()
    assert data == {
        'title': 'ticker_02',
        'rates': [
            {'rate': 1, 'timestamp': '2020-07-01T12:00:00'},
            {'rate': 0, 'timestamp': '2020-07-01T13:00:00'},
            {'rate': 1, 'timestamp': '2020-07-01T14:00:00'},
            {'rate': 2, 'timestamp': '2020-07-01T10:00:00'},
            {'rate': 1, 'timestamp': '2020-07-02T12:00:00'},
        ],
    }


def test_app_get_point_id_error_times(mocker) -> None:
    """get_coin: start_time больше end_time.
    """
    mocker.patch('app.api.coins.database.fetch_all', fake_response('get_coin_ok'))
    mocker.patch('app.api.coins.datetime')
    coins.datetime.now.return_value = datetime(2020, 7, 1, 12)

    response = client.get('/api/v1/coins/22?start_time=2021-06-01T12:00:00&end_time=2020-07-01T12:00:00')
    assert response.status_code == 400, response.text
    data = response.json()
    assert data == {'detail': '"start_time" is upper "end_time"'}


def test_app_get_point_id_empty(mocker) -> None:
    """get_coin: Нет истории по монете с запрошенным id.
    """
    mocker.patch('app.api.coins.database.fetch_all', fake_response('get_coin_empty'))
    mocker.patch('app.api.coins.datetime')
    coins.datetime.now.return_value = datetime(2020, 7, 1, 12)

    response = client.get('/api/v1/coins/22')
    assert response.status_code == 404, response.text
    data = response.json()
    assert data == {'detail': 'Coind with id `22` not found'}

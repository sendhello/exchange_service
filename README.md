# Exchange Service

### Зависимости
* python 3.10+

### Установка зависимостей
Установка `poetry` (если отсутствует)
```commandline
pip install poetry
```
Добавление виртуального окружения и установка зависимостей
```commandline
poetry install --no-dev
```
То же самое с утилитами разработки
```commandline
poetry install
```

### Запуск сервиса
##### в виртуальном окружении:
```shell
uvicorn app.main:app --host "0.0.0.0" --port "3000"
```
##### в docker-compose:
```shell
docker-compose -f deploy/docker/docker-compose.yml up
```

### Документация
* http://127.0.0.1:3000/docs (Swagger)
* http://127.0.0.1:3000/redoc (Redoc)

### Миграции
###### Создание миграции
```shell
alembic revision --autogenerate -m "message"
```
###### Применение миграции
```shell
alembic upgrade head
```
###### Откат миграции
```shell
alembic downgread <hash_migrate>
```

### Запуск unit-тестов
```commandline
pytest -svvx
```

### Переменные окружения
* `DATABASE_URL` - строка подключения к базе данных

## API

### Получение списка монет
###### Request
```json
GET http://localhost:3001/api/v1/coins
```
###### Response
```json
[
    {
        "current_rate": 57,
        "id": 1,
        "title": "ticker_00"
    },
    ...
]
```

### Получение данных монеты по ее ID
###### Request
```json
GET http://localhost:3001/api/v1/coins/<coin_id>
```
###### Response
```json
{
    "title": "ticker_02",
    "rates": [
        {"rate": 1, "timestamp": "2020-07-01T12:00:00"},
        {"rate": 0, "timestamp": "2020-07-01T13:00:00"},
        {"rate": 1, "timestamp": "2020-07-01T14:00:00"},
        {"rate": 2, "timestamp": "2020-07-01T10:00:00"},
        {"rate": 1, "timestamp": "2020-07-02T12:00:00"},
    ]
}
```

### Healthcheck
###### Request
```json
POST http://localhost:3001/api/v1/healthcheck
```
###### Response
```json
{
  "status": "alive"
}
```

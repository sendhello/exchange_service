import asyncio
import sys
from datetime import datetime
from decimal import Decimal
from time import sleep

sys.path = ['', '..'] + sys.path[1:]

from app.db import database
from app.db.queries import get_coins_query, set_coins_query, set_rates_query
from app.schemas import CoinDb, Rate


async def generate_coins() -> None:
    """Генерация искусственных монет.
    """
    # Проверяем наличие сгенерированных монет в БД
    db_coins = await database.fetch_all(get_coins_query())

    # Если монет нет, то генерируем новые
    if not db_coins:
        await database.execute_many(query=set_coins_query(), values=[{'title': f'ticker_{i:02}'} for i in range(100)])


async def start_tickers() -> None:
    """Генерация курсов.
    """
    while True:
        start_time = datetime.now()

        db_coins = await database.fetch_all(get_coins_query())
        for db_coin in db_coins:
            coin = CoinDb.parse_obj(db_coin)
            if coin.current_rate is None:
                coin.current_rate = Decimal(0)

            rate = Rate(
                coin_id=coin.id,
                timestamp=datetime.utcnow(),
                rate=coin.current_rate,
            )
            rate.update_rate()
            await database.execute(query=set_rates_query(), values=rate.dict())

        # Делаем паузу между коррекциями курсов ровно 1 секунду
        # или время выполнение запроса, если оно более 1 секунды.
        run_time = (datetime.now() - start_time).total_seconds()
        sleep(1 - run_time if run_time < 1 else 0)


async def main() -> None:
    await database.connect()
    await generate_coins()
    await start_tickers()


if __name__ == '__main__':
    asyncio.run(main())
